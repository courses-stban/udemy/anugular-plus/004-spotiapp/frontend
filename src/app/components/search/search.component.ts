import { Component } from '@angular/core';
import { SpotifyService } from '../../services/spotify.service';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styles: []
})
export class SearchComponent {
  artists: any[] = [];
  loading: boolean;

  constructor(private spotifyService: SpotifyService) { }

  buscar(termino: string) {
    this.loading = true;
    this.spotifyService.getToken()
      .subscribe(dataToken => {
        this.getInfo(termino);
      });
  }

  getInfo(termino: string) {
    this.spotifyService.getArtists(termino)
      .subscribe(
        (data: any) => {
          this.artists = data;
          this.loading = false;
        },
        (error: any) => this.loading = false
      );
  }
}
