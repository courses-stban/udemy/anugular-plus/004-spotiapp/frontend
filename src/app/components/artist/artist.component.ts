import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { SpotifyService } from '../../services/spotify.service';

@Component({
  selector: 'app-artist',
  templateUrl: './artist.component.html'
})
export class ArtistComponent {
  artist: any = {};
  loading: boolean;
  topTracks: any[];

  constructor(
    private activatedRoute: ActivatedRoute,
    private spotifyService: SpotifyService) {
    this.loading = true;
    this.spotifyService.getToken()
      .subscribe(dataToken => {
        this.getInfo();
      });
  }

  getInfo() {
    this.activatedRoute.params.subscribe( params => {
      this.getArtist(params.id);
      this.getTopTracks(params.id);
    });
  }

  getArtist(id: string) {
    this.spotifyService.getArtist(id)
      .subscribe( (data: any) => {
        this.artist = data;
        this.loading = false;
        console.log(this.artist);
      });
  }

  getTopTracks(id: string) {
    this.spotifyService.getTopTracks(id)
      .subscribe( (data: any) => {
        this.topTracks = data;
        console.log(this.topTracks);
      });
  }
}
