import { Component } from '@angular/core';
import { SpotifyService } from '../../services/spotify.service';
import swal from 'sweetalert';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styles: []
})
export class HomeComponent {
  newReleases: any[] = [];
  loading: boolean;
  messageError: string;

  constructor(private spotifyService: SpotifyService) {
    this.spotifyService.getToken()
      .subscribe(dataToken => {
        this.getNewReleases();
      });
  }

  getNewReleases() {
    this.loading = true;
    this.spotifyService.getNewReleases()
      .subscribe(
        (data: any) => {
          this.newReleases = data;
          this.loading = false;
        },
        (error: any) => {
          this.loading = false;
          this.messageError = error.error.error.message;
          swal({
            title: 'Error!',
            text: this.messageError,
            icon: 'error',
            buttons: {}
          });
        }
      );
  }

}
