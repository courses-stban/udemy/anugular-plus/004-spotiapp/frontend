import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class SpotifyService {
  token: string;

  constructor(private http: HttpClient) { }

  getToken() {
    const client_id = 'bd0f4dc92c8f44138eb59e580565b7e2';
    const client_secret = 'a540dc10247e426cac8378627fad5795';
    const url = `http://localhost:3000/spotify/${client_id}/${client_secret}`;
    return this.http.get(url)
      .pipe( map(data => {
        this.token = data['access_token'];
      }));
  }

  getQuery(query: string) {
    const url = `https://api.spotify.com/v1/${ query }`;

    const headers = new HttpHeaders({
      Authorization: `Bearer ${ this.token }`
    });

    return this.http.get(url, { headers });
  }

  getNewReleases() {
    return this.getQuery('browse/new-releases')
      .pipe( map( (data: any) => data.albums.items ));
  }

  getArtists(termino: string) {
    return this.getQuery(`search?q=${ termino }&type=artist&limit=15`)
      .pipe( map( (data: any) => data.artists.items ));
  }

  getArtist(id: string) {
    return this.getQuery(`artists/${ id }`);
  }

  getTopTracks(id: string) {
    return this.getQuery(`artists/${ id }/top-tracks?country=us`)
      .pipe( map( (data: any) => data.tracks));
  }
}
